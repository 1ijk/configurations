# configuration

custom nix configurations for my development workflow

```shell
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
nix --version
nix develop "gitlab:1ijk/configurations"
```