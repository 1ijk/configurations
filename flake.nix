{
  description = "Master Flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = { self, nixpkgs }:
    let
      allSystems = [
        "x86_64-linux" # 64-bit Intel/AMD Linux
        "aarch64-linux" # 64-bit ARM Linux
        "x86_64-darwin" # 64-bit Intel macOS
        "aarch64-darwin" # 64-bit ARM macOS
      ];

      forAllSystems = f: nixpkgs.lib.genAttrs allSystems (system: f {
        pkgs = import nixpkgs { inherit system; };
      });
      in
      {
        devShells = forAllSystems ({ pkgs }:
          let
            common = with pkgs; [
              git
              grpc
              grpc-gateway
              grpcurl
              jq
              openssl
              protobuf
              protoc-gen-doc
              vim
            ];

            python = pkgs.python311;
          in {
            default = pkgs.mkShell {
              packages = common;

              shellHook = ''
                echo "Default Shell"
                echo "available development environments:\n"
                echo "  #go"
                echo "  #rust"
                echo "  #devops"
                echo "  #data"
                echo "  #blog"
                echo "\ne.g. run 'nix develop gitlab:1ijk/configurations#rust'"
              '';
            };

            go = pkgs.mkShell {
              packages = with pkgs; [
                go_1_20
                gosec
                gotools
                protoc-gen-go
              ] ++ common;

              shellHook = ''
                echo "Go Language Shell 🟢"
              '';
            };

            rust = pkgs.mkShell {
              packages = with pkgs; [
                cargo
                protoc-gen-rust
                rustc
              ] ++ common;

              shellHook = ''
                echo "Rust Language Shell 🦀"
              '';
            };

            devops = pkgs.mkShell {
              packages = with pkgs; [
                awscli2
                bazel_6
                docker
                terraform
              ] ++ common;

              shellHook = ''
                echo "Dev/Ops Shell 👷"
              '';
            };

            data = pkgs.mkShell {
              packages = with pkgs; [
                (python.withPackages (ps: with ps; [
                                  ipython
                                  jupyter
                                  pip
                                  virtualenv
                                ]))
              ] ++ common;

              shellHook = ''
                echo "Data Science Shell 🔬"
              '';
            };

            blog = pkgs.mkShell {
              packages = with pkgs; [
                hugo
              ] ++ common;

              shellHook = ''
                echo "Blogging Shell 📝"
              '';
            };
          });
        };
}
